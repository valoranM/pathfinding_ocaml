let rec load_terrain murs n =
  let traversable xP yP =
    List.exists (fun (x, y, w, h) ->
        xP >= x && yP >= y && xP < x + w && yP < y + h)
  in
  let rec ligne inc acc =
    if inc > n then
      acc
    else
      ligne (inc + 1)
        (Array.append acc
           [| Array.init n (fun i -> not (traversable i inc murs)) |])
  in
  ligne 0 [||]

let find_path (xDep, yDep) (xArr, yArr) terrain =
  Printf.printf "%d\n" Sys.max_array_length;
  let n = Array.length terrain - 1 in
  let visite = Array.make_matrix n n false in
  let preds = Array.make_matrix n n (-1, -1) in
  let file = Queue.create () in
  let ajoute (x, y) (xp, yp) =
    Queue.add (x, y) file;
    visite.(y).(x) <- true;
    preds.(y).(x) <- (xp, yp)
  in
  ajoute (xDep, yDep) (-1, -1);
  while not (Queue.is_empty file) do
    let x, y = Queue.take file in
    List.iter
      (fun (xp, yp) ->
        if
          xp >= 0 && xp < n && yp >= 0 && yp < n
          && (not visite.(yp).(xp))
          && terrain.(yp).(xp)
        then
          ajoute (xp, yp) (x, y))
      [ (x, y - 1); (x - 1, y); (x, y + 1); (x + 1, y) ]
  done;
  let rec loop (x, y) path =
    if (xDep, yDep) = (x, y) then
      (xDep, yDep) :: path
    else
      loop preds.(y).(x) ((x, y) :: path)
  in
  loop (xArr, yArr) []

open Scanf
open Printf

let load file =
  let c = Scanning.open_in file in
  let n = bscanf c "%d\n" (fun n -> n) in
  let r = bscanf c "%d\n" (fun r -> r) in
  let murs = ref [] in
  for _ = 1 to r do
    bscanf c "%d %d %d %d\n" (fun x y dx dy -> murs := (x, y, dx, dy) :: !murs)
  done;
  (!murs, n)

let print_path terrain n path =
  print_endline (String.make (n + 2) '@');
  for j = n - 1 downto 0 do
    print_char '@';
    for i = 0 to n - 1 do
      if List.exists (fun (x, y) -> x = i && y = j) path then
        print_char '0'
      else if terrain.(j).(i) then
        print_char ' '
      else
        print_char '#'
    done;
    print_endline "@"
  done;
  print_endline (String.make (n + 2) '@')

let _ =
  let file = Sys.argv.(1) in
  let murs, n = load file in
  let terrain = load_terrain murs n in
  let t3 = Unix.gettimeofday () in
  let path = find_path (n / 2, n - 1) (n / 2, 0) terrain in
  let t4 = Unix.gettimeofday () in
  Printf.printf "Temps:\n  recherche de chemin %fs\n" (t4 -. t3);
  print_path terrain n path
