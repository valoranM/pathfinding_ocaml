EXEC=pathfinding
EXEC_DIR=src

.PHONY: all
all: $(EXEC)

$(EXEC):
	@(cd $(EXEC_DIR) && $(MAKE))
	
.PHONY: clean mrproper $(EXEC)
clean:
	@(cd $(EXEC_DIR) && $(MAKE) $@)

mrproper: clean
	@(cd $(EXEC_DIR) && $(MAKE) $@)
