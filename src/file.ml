module File = struct
  type file = (int * float) list ref

  let vide () : file = ref []
  let est_vide (q : file) = !q = []

  let add x prio (q : file) =
    if List.exists (fun (x', _) -> x' = x) !q then
      ()
    else
      q := (x, prio) :: !q

  let trouve_min q =
    let rec trouve_min_aux (x : int) (prio : float) q =
      match q with
      | [] -> (x, prio)
      | (x', prio') :: q when prio' > prio -> trouve_min_aux x prio q
      | (x', prio') :: q -> trouve_min_aux x' prio' q
    in
    match !q with
    | [] -> failwith "trouve_min: la file est vide"
    | (v, clef) :: q -> trouve_min_aux v clef q

  let rec supprime x q =
    match q with
    | [] -> []
    | (v, _) :: q when v = x -> q
    | (v, clef) :: q -> (v, clef) :: supprime x q

  let suprime_min q =
    if !q = [] then
      failwith "extraire_min: file vide"
    else
      let min = trouve_min q in
      q := supprime (fst min) !q;
      min
end
