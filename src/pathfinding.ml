open Quadtree

let rec draw_path acc preds coords coef =
  let pred = preds.(acc) in
  if pred != acc then (
    Graphics.set_color 0xff0000;
    let x1, y1 = coords.(acc) in
    let x2, y2 = coords.(pred) in
    Graphics.fill_circle
      (truncate (x1 *. float coef))
      (truncate (y1 *. float coef))
      4;
    Graphics.fill_circle
      (truncate (x2 *. float coef))
      (truncate (y2 *. float coef))
      4;
    Graphics.draw_segments
      [|
        ( truncate (x1 *. float coef),
          truncate (y1 *. float coef),
          truncate (x2 *. float coef),
          truncate (y2 *. float coef) );
      |];
    draw_path pred preds coords coef
  )

let draw size coef coords g qt acc preds =
  Graphics.open_graph " 512x512";
  Qtree.draw size coef coords g qt;
  draw_path acc preds coords coef;
  ignore (Graphics.read_key ())

(**
   Fonction de recherche d'un chemin.
   Applique un algorithme de recherche et reconstruit le trouvé.
   Renvoie la liste des paires de coordonnées formant le chemin.

   Arguments :
   - (xDep, yDep) les coordonnées du point de départ
   - (xArr, yArr) les coordonnées du point d'arrivée
   - (qt, n) le quadtree et la longueur du côté du terrain
   - (g, coords) le graphe et le tableau des coordonnées
 *)
let find_path (xDep, yDep) (xArr, yArr) (qt, n) (g, coords) =
  let d = Qtree.plus_proche (xDep, yDep) coords qt in
  let f = Qtree.plus_proche (xArr, yArr) coords qt in
  let dist, preds = Graph.Graph.dijkstra d g in
  let rec find_aux pred acc =
    if preds.(pred) == pred then
      acc
    else
      find_aux preds.(pred) (coords.(pred) :: acc)
  in
  (find_aux f [], d, f, preds)

let print_path p =
  List.iter (fun (x, y) -> Printf.printf "\t\t(%.3f, %.3f)\n" x y) p;
  Printf.printf "\n"

open Scanf

(**
   Fonction de lecture du fichier d'entrée
     load: string -> (int * int) list * int

   L'appel  (load f)  lit le fichier dont le nom est donné par la 
   chaîne (f) et renvoie une paire  (murs, n)  où
   - (murs) est la liste des quadruplets (x, y, dx, dy) donnant les
     dimensions des r zones intraversables
   - (n) est la longueur du côté du terrain
   On suppose que le fichier (f) contient une description de terrain
   valide. Le résultat de (load) n'est pas spécifié sinon.
 *)
let load file =
  let c = Scanning.open_in file in
  let n = bscanf c "%d\n" (fun n -> n) in
  let r = bscanf c "%d\n" (fun r -> r) in
  let murs = ref [] in
  for _ = 1 to r do
    bscanf c "%d %d %d %d\n" (fun x y dx dy -> murs := (x, y, dx, dy) :: !murs)
  done;
  (!murs, n)

let _ =
  let file = Sys.argv.(1) in
  let murs, n = load file in
  let t1 = Unix.gettimeofday () in
  let qt, k = Qtree.numerote (Qtree.list2qtree n murs 0 0) 0 in
  let t2 = Unix.gettimeofday () in
  let coords = Qtree.mk_coords qt k n in
  let g = Qtree.mk_graph qt coords in
  let t3 = Unix.gettimeofday () in
  let path, d, f, preds =
    find_path (float n /. 2., 0.) (float n /. 2., float n) (qt, n) (g, coords)
  in
  let t4 = Unix.gettimeofday () in
  Printf.printf
    "Temps:\n\
    \  construction du quadtree %fs\n\
    \  construction du graphe %fs\n\
    \  recherche de chemin %fs\n"
    (t2 -. t1) (t3 -. t2) (t4 -. t3);
  print_path path;
  draw 512 (512 / n) coords g qt f preds
