module Graph :
  sig
    type graph = (int * float) list array
    val print_graph : (int * float) list array -> unit
    val print_graph_compact : (int * float) list array -> unit
    val add : int * float -> int -> graph -> unit
    val empty : int -> 'a list array
    val dijkstra : int -> graph -> float array * int array
    val draw :
      int -> int -> (int * 'a) list array -> (float * float) array -> unit
  end
