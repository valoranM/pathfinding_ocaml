module Qtree = struct
  type qtree =
      | Libre of int (* numéro *)
      | Mur
      | Quad of qtree * qtree * qtree * qtree

  (**
   Fonction de numérotation des quadtrees
     numerote: qtree -> int -> qtree * int

   L'appel  (numerote qt k)  renvoie une paire  (qt', k')  où 
   - (qt') est un quadtree de même structure que (qt) mais dont les 
     régions libres sont numérotées consécutivement à partir de (k)
   - (k') est l'entier qui suit le dernier numéro utilisé
  *)
  let rec numerote qt k =
    match qt with
    | Libre _ -> (Libre k, k + 1)
    | Mur -> (Mur, k)
    | Quad (no, ne, so, se) ->
        let no, k = numerote no k in
        let ne, k = numerote ne k in
        let so, k = numerote so k in
        let se, k = numerote se k in
        (Quad (no, ne, so, se), k)

  (**
   Affichage d'un quadtree 
   (vue hiérarchique avec retrait proportionnel à la profondeur)
  *)
  let print_qtree qt =
    let offset n = String.make n ' ' in
    let rec print o = function
      | Mur -> Printf.printf "%sMur\n" (offset o)
      | Libre k -> Printf.printf "%sLibre %d\n" (offset o) k
      | Quad (no, ne, so, se) ->
          Printf.printf "%sQuad\n" (offset o);
          print (o + 2) no;
          print (o + 2) ne;
          print (o + 2) so;
          print (o + 2) se
    in
    print 0 qt

  (**
   Fonction de construction d'un quadtree à partir d'une liste de
   régions intraversables
     list2qtree: int -> (int * int * int * int) list -> qtree

   Arguments : 
   - (n) est la longueur du côté du terrain
   - (l) est la liste des rectangles intraversables
  *)
  let rec list2qtree n l x y =
    let intersect =
      List.filter (fun (xm, ym, w, h) ->
          x < xm + w && x + n > xm && y < ym + h && y + n > ym)
    in
    let l = intersect l in
    match l with
    | [] -> Libre (-1)
    | _ ->
        if n = 1 then
          Mur
        else
          let recouvrement =
            List.exists (fun (xm, ym, w, h) ->
                xm <= x && xm + w >= x + n && ym <= y && ym + h >= y + n)
          in
          if recouvrement l then
            Mur
          else
            Quad
              ( list2qtree (n / 2) l x (y + (n / 2)),
                list2qtree (n / 2) l (x + (n / 2)) (y + (n / 2)),
                list2qtree (n / 2) l x y,
                list2qtree (n / 2) l (x + (n / 2)) y )

  (**
   Fonction de calcul des coordonnées des centres des régions libres.
   Renvoie un tableau de paires de coordonnées.
   
   Arguments :
   - (qt) le quadtree
   - (k) le nombre de régions libres
   - (n) la longueur du côté du terrain
   Pré-condition :
   - les régions doivent être numérotées de 0 à k-1
 *)
  let mk_coords qt k n =
    let coord = Array.make k (0., 0.) in
    let rec mk_coords_rec n (x, y) = function
      | Mur -> ()
      | Libre k -> coord.(k) <- (x, y)
      | Quad (no, ne, so, se) ->
          mk_coords_rec (n /. 2.) (x -. (n /. 2.), y +. (n /. 2.)) no;
          mk_coords_rec (n /. 2.) (x +. (n /. 2.), y +. (n /. 2.)) ne;
          mk_coords_rec (n /. 2.) (x -. (n /. 2.), y -. (n /. 2.)) so;
          mk_coords_rec (n /. 2.) (x +. (n /. 2.), y -. (n /. 2.)) se
    in
    mk_coords_rec (float n /. 2.) (float n /. 2., float n /. 2.) qt;
    coord

  let dist (x1, y1) (x2, y2) =
    sqrt (((x2 -. x1) *. (x2 -. x1)) +. ((y2 -. y1) *. (y2 -. y1)))

  open Graph

  (**
   Fonction de construction d'un graphe pondéré à partir d'un quadtree.
   Renvoie un graphe.

   Arguments :
   - (qt) le quadtree
   - (coords) le tableau des coordonnées
  *)
  let mk_graph qt coords =
    let graph = Graph.empty (Array.length coords) in
    let rec interface_horizontal qt1 qt2 =
      match qt1 with
      | Libre k1 -> (
          match qt2 with
          | Libre k2 ->
              let d = dist coords.(k1) coords.(k2) in
              Graph.add (k2, d) k1 graph;
              Graph.add (k1, d) k2 graph
          | Quad (no, _, so, _) ->
              interface_horizontal qt1 no;
              interface_horizontal qt1 so
          | _ -> ())
      | Quad (no, ne, so, se) -> (
          enumere_quad qt1;
          match qt2 with
          | Libre k2 ->
              interface_horizontal ne qt2;
              interface_horizontal se qt2
          | Quad (no2, ne2, so2, se2) ->
              interface_horizontal ne no2;
              interface_horizontal se so2;
              enumere_quad qt2
          | _ -> ())
      | _ -> ()
    and interface_vertical qt1 qt2 =
      match qt1 with
      | Libre k1 -> (
          match qt2 with
          | Libre k2 ->
              let d = dist coords.(k1) coords.(k2) in
              Graph.add (k2, d) k1 graph;
              Graph.add (k1, d) k2 graph
          | Quad (no, ne, _, _) ->
              interface_vertical qt1 no;
              interface_vertical qt1 ne
          | _ -> ())
      | Quad (no, ne, so, se) -> (
          match qt2 with
          | Libre k2 ->
              interface_vertical so qt2;
              interface_vertical se qt2
          | Quad (no2, ne2, _, _) ->
              interface_vertical so no2;
              interface_vertical se ne2
          | _ -> ())
      | _ -> ()
    and enumere_quad qt =
      match qt with
      | Quad (no, ne, so, se) ->
          interface_horizontal no ne;
          interface_horizontal so se;
          interface_vertical no so;
          interface_vertical ne se;
          ()
      | _ -> ()
    in
    enumere_quad qt;
    graph

  let draw n coef coords g qt =
    let rec draw_rec x y n coef coords g = function
      | Libre k ->
          Graphics.draw_rect x y n n;
          let xd, yd = coords.(k) in
          Graphics.fill_circle
            (truncate (xd *. float coef))
            (truncate (yd *. float coef))
            4;
          Graph.draw k coef g coords
      | Mur -> Graphics.fill_rect x y n n
      | Quad (no, ne, so, se) ->
          let w = n / 2 in
          draw_rec x y w coef coords g so;
          draw_rec (x + w) y w coef coords g se;
          draw_rec x (y + w) w coef coords g no;
          draw_rec (x + w) (y + w) w coef coords g ne
    in
    draw_rec 0 0 n coef coords g qt

  let plus_proche (x, y) coords qt =
    let kn = ref Int.min_int in
    let dmin = ref Float.max_float in
    let rec plus_proche_aux = function
      | Mur -> ()
      | Libre k ->
          if dist (x, y) coords.(k) < !dmin then (
            kn := k;
            dmin := dist (x, y) coords.(k)
          )
      | Quad (no, ne, so, se) ->
          plus_proche_aux no;
          plus_proche_aux ne;
          plus_proche_aux so;
          plus_proche_aux se
    in
    plus_proche_aux qt;
    !kn
end
