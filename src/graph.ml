module Graph = struct
  type graph = (int * float) list array

  let print_graph g =
    let n = Array.length g in
    Printf.printf "Graphe à %d sommets :\n" n;
    for i = 0 to n - 1 do
      Printf.printf "Sommet %d:\n" i;
      List.iter
        (fun (v, d) -> Printf.printf "  voisin %d à distance %f\n" v d)
        g.(i)
    done

  let print_graph_compact g =
    for i = 0 to Array.length g - 1 do
      Printf.printf "%d:" i;
      List.iter (fun (v, f) -> Printf.printf " %d %f" v f) g.(i);
      Printf.printf "\n"
    done

  let add (item : int * float) k (g : graph) = g.(k) <- item :: g.(k)
  let empty k = Array.make k []

  open File

  let dijkstra d (g : graph) =
    let n = Array.length g in
    let vus = Array.make n false in
    let dist = Array.make n 999. in
    let pred = Array.make n (-1) in
    let file = File.vide () in
    let ajoute s e d =
      dist.(s) <- d;
      pred.(s) <- e;
      File.add s d file
    in
    ajoute d d 0.;
    while not (File.est_vide file) do
      let s, ds = File.suprime_min file in
      if not vus.(s) then (
        vus.(s) <- true;
        List.iter
          (fun (v, dsv) ->
            let d = ds +. dsv in
            if d < dist.(v) then ajoute v s d)
          g.(s)
      )
    done;
    (dist, pred)

  let draw k coef g coords =
    let xd, yd = coords.(k) in
    List.iter
      (fun (k, _) ->
        let xn, yn = coords.(k) in
        Graphics.draw_segments
          [|
            ( truncate (xd *. float coef),
              truncate (yd *. float coef),
              truncate (xn *. float coef),
              truncate (yn *. float coef) );
          |])
      g.(k)
end
