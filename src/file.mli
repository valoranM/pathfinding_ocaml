module File :
  sig
    type file = (int * float) list ref
    val vide : unit -> file
    val est_vide : file -> bool
    val add : int -> float -> file -> unit
    val suprime_min : (int * float) list ref -> int * float
  end
