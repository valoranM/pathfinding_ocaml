module Qtree :
  sig
    type qtree = Libre of int | Mur | Quad of qtree * qtree * qtree * qtree
    val numerote : qtree -> int -> qtree * int
    val print_qtree : qtree -> unit
    val list2qtree :
      int -> (int * int * int * int) list -> int -> int -> qtree
    val mk_coords : qtree -> int -> int -> (float * float) array
    val dist : float * float -> float * float -> float
    val mk_graph : qtree -> (float * float) array -> (int * float) list array
    val draw :
      int ->
      int -> (float * float) array -> (int * 'a) list array -> qtree -> unit
    val plus_proche : float * float -> (float * float) array -> qtree -> int
  end
